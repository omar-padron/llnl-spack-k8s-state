# llnl-spack-k8s-state

Maintains the state of the LLNL Spack k8s cluster using [flux](https://fluxcd.io/).

Create the flux namespace and flux operator:

```console
 $ ./bootstrap/bootstraph.sh
```
